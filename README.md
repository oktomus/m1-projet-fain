------------------

- **Projet FAIN**
- M1 S1 ISI -- 2017
- Kevin Masson

------------------

## Démo

![Project demo](doc/demo.gif)

## Utilisation

    make
    ./Plot 500 500
    # Ajout des points avec clic gauche
    # Remplissage du polygone en appuyant sur f
    # Echap pour quitter

## Fonctionnalités

Toutes les fonctionnalités du sujet ont été implémentées. 
On peut aussi dessiner des polygones complexes, et la selection d'arrête n'est pas naïve (je me base sur les deux points de l'arrête et non sur le point le plus près).

- Global
    - Remplissage du polygone en appuyant sur `f`
    - Fermeture du polygone en appuyant sur `c`
- Mode 'a', Append
    - Ajout de point avec le clic gauche
- Mode 'v', Vertex
    - Selection d'une vertex avec le click gauche ou `Page down` et `Page up`
    - Déplacement de la vertex selectionné avec les flèches directionnelles 
    - Suppression du point selectionné avec `Delete`
- Mode 'e', Edges
    - Selection d'une edge avec le click gauche ou `Page down` et `Page up`
    - Split de l'arrête avec click molette

## Algorithme de remplissage

`src/Polygon.c PO_Fill ligne 232`

J'ai utilisé l'algorithme scanline pour le remplissage de polygone. Il fonctionne briévement de la manière suivante:

- **Pour chaque ligne**
    - Calculer la liste des intersections avec les segments
    - Initialisation d'un compteur à 0
    - **Pour chaque pixel horizontal (pour chaque colonne)**
        - Si le pixel intersecte un segment, incrémenter le compteur
        - Si le pixel est un sommet
            - Regarder la position des sommets adjacents
            - Et incrémenter le compteur si besoin
        - Dessiner le pixel si la valeur du compteur est paire

Ce n'est pas le premier algorithme que j'ai implémanté. Au départ, je testais l'intersection pour chaque pixel de chaque ligne. Mais du fais que les coordonnées sont stockés avec des entiers, les résultats n'étaient pas convaicants et suivant la pente des segments, des intersections étaient mal calculées.

Ensuite, j'ai voulu utiliser une liste d'intersections ordonnées en X et dessiner un ligne en utilisant un pair de point sur deux. Mais cela posait problème quand il y avait un ou plusieurs sommets sur la même ligne. 

