CC=gcc

CFLAGS=-Wall -g
LFLAGS=-L/usr/lib -lm -lGL -lglut -lGLEW

EXEC=Plot
SRCS=$(wildcard src/*.c)
HEADS=$(wildcard src/*.h)
OBJS=$(SRCS:.c=.o)

$(EXEC) : $(OBJS)
	$(CC) $^ -o $@ $(LFLAGS)

%.o : %.c %.h
	$(CC) -c $< -o $@ $(CFLAGS)

clean :
	/bin/rm $(EXEC) *.o
