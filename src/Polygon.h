#ifndef __POLYGON__
#define __POLYGON__

#include <stdbool.h>

#include "Image.h"

/**
  * @breif A pixel, a point in an image
  * 0,0 being the bottom and the left
  */
typedef struct{
    int x;
    int y;
} Pixel;

/**
  * @brief A list of pixel
  * Double chained list
  */
typedef struct st_pixlist{
    Pixel p;
    struct st_pixlist* next;
    struct st_pixlist* previous;
} StructPixelList, *PixelList;

typedef struct{
    PixelList pixels; // A pointer !
    PixelList pixelsEnd; // Last pixel
    PixelList select; // Current selection
    Color color; // Color draw
    bool closed; // If closed or not
    bool fill; // To fill or not
    int count; // Nb of point
    // BBox
    int ymin;
    int ymax;
    int xmin;
    int xmax;
    // Selection mode as vertex or edge 'v', 'e'
    char selectionMode;
} StructPolygon, *Polygon;

// --------- Pixel list

/**
  * @brief Allocate a new list
  */
PixelList PL_allocate();

/**
  * @brief Free the memory used by the given list
  */
void PL_free(PixelList);

/**
  * @brief Add a pixel in the given list.
  * The given list is a pointer of pointer.
  * Will allocate if list null.
  */
void PL_addPixel(PixelList*, int, int);

/**
  * @brief Sort the given pixel list by x coordinates
  */
PixelList PL_sortByX(PixelList);

/**
  * @brief Print the given list
  */
void PL_print(PixelList);

/**
  * @brief Tells if the given list contains the given point
  */
bool PL_contains(PixelList, Pixel*);

/**
  * @brief Returns the number occurence of the given pixel in the list
  */
int PL_count(PixelList, Pixel*);

// --------- Polygon

/**
  * @brief Allocate a polygon
  */
Polygon PO_allocate();

/**
  * @brief Free the memory used by the given polygon
  */
void PO_free(Polygon);

/**
  * @brief Add a pixel in the given polygon
  */
void PO_addPixel(Polygon, int, int);

/**
  * @brief Draw the given polygon
  */
void PO_draw(Polygon, Image*);

/**
  * @brief Draw a box over the selected point
  */
void PO_drawSelected(Polygon, Image*);

/**
  * @brief Fill the given polygon in the given image
  */
void PO_fill(Polygon, Image*);

/**
  * @brief Return the list of edge intersection points with the given polygon
  * on the given row
  */
PixelList PO_intersectionPoints(Polygon, int);

/**
  * @brief Select the next point in the point list
  */
void PO_selectNext(Polygon);

/**
  * @brief Select the previous point in the point list
  */
void PO_selectPrevious(Polygon);

/**
  * @brief Select the closest point from the given vertex
  */
void PO_selectClosestVertex(Polygon, int, int);

/**
  * @brief Select the closest edge
  */
void PO_selectClosestEdge(Polygon, int, int);

/**
  * @brief Translated the selected point by the given amount
  */
void PO_translateSelected(Polygon, int, int);

/**
  * @brief Remove the selected point from the polygon
  */
void PO_deleteSelected(Polygon);

/**
  * @brief Split the currently selected edge
  */
void PO_splitSelectedEdge(Polygon);

/**
  * @brief Returns the closet point from the given point in the given polygon
  */
PixelList PO_closestVertex(Polygon, int, int);

// --------- Image

/**
  * @brief Draw a line from a given point to another
  */
void IM_drawLine(Image *, int, int, int, int);

// --------- Utils

/**
  * @brief Checks if two seg are intersecting.
  * Output to the last arg if so
  */
bool isSegementIntersecting(Pixel, Pixel, Pixel, Pixel, Pixel*);

#endif
