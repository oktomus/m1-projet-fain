#include "Polygon.h"

#include <stdio.h>
#include <stdlib.h>

// --------- Utils

static bool PX_equal(Pixel* a, Pixel *b)
{
    return a->x == b->x && a->y == b->y;
}

// --------- Pixel list

PixelList PL_allocate()
{
    PixelList pl = malloc(sizeof(StructPixelList));
    pl->next = NULL;
    pl->previous = NULL;
    return pl;
}

void PL_free(PixelList pl)
{
    while(pl != NULL)
    {
        PixelList next = pl->next;
        free(pl);
        pl = next;
    }
}

void PL_addPixel(PixelList* head, int x, int y)
{
    Pixel pix = {x, y};
    if((*head) == NULL)
    {
        (*head) = PL_allocate();
        (*head)->p = pix;
        return;
    }
    PixelList last = (*head);
    while(last->next != NULL)
        last = last->next;
    last->next = PL_allocate();
    last->next->p = pix;
    last->next->previous = last;
}

static PixelList PL_mergeSortX(PixelList a, PixelList b)
{
    PixelList newhead = NULL;
    if(a == NULL) return b;
    if(b == NULL) return a;

    if(a->p.x < b->p.x)
    {
        newhead = a;
        newhead->next = PL_mergeSortX(a->next, b);
    }
    else
    {
        newhead = b;
        newhead->next = PL_mergeSortX(a, b->next);
    }

    return newhead;
}

PixelList PL_sortByX(PixelList head)
{
    if(head == NULL || head->next == NULL) return head;

    PixelList top = head;
    // Find last
    PixelList last = head->next;
    while(last != NULL && last->next != NULL)
    {
        head = head->next;
        last = head->next->next;
        // Double jump
    }
    last = head->next;
    head->next = NULL;

    return PL_mergeSortX(PL_sortByX(top), PL_sortByX(last));
}

void PL_print(PixelList list)
{
    fprintf(stdout, "PixelList[");
    while(list != NULL)
    {
        fprintf(stdout, "[%d,%d]", list->p.x, list->p.y);
        if(list->next != NULL) fprintf(stdout, ", ");
        list = list->next;
    }
    fprintf(stdout, "]\n");
}

bool PL_contains(PixelList plist, Pixel* p)
{
    while(plist != NULL)
    {
        if(PX_equal(p, &(plist->p))) return true;
        plist = plist->next;

    }
    return false;
}

int PL_count(PixelList plist, Pixel*p)
{
    int res = 0;
    while(plist != NULL)
    {
        if(PX_equal(p, &(plist->p))) res++;
        plist = plist->next;

    }
    return res;
}

// --------- Polygon

Polygon PO_allocate()
{
    Polygon p = malloc(sizeof(StructPolygon));
    p->color = C_new(1, 1, 1);
    p->pixels = NULL;
    p->pixelsEnd = NULL;
    p->select = NULL;
    p->closed = false;
    p->fill = false;
    p->count = 0;
    p->ymin = 0;
    p->ymax = 0;
    p->xmin = 0;
    p->xmax = 0;
    p->selectionMode = 'v';
    return p;
}

void PO_free(Polygon p)
{
    if(p==NULL) return;
    PL_free(p->pixels);
    free(p);
}

void PO_addPixel(Polygon p, int x, int y)
{
    PL_addPixel(&(p->pixels), x, y);
    p->count++;

    // BBox
    if(p->count == 1)
    {
        p->ymin = y; p->ymax = y;
        p->xmin = x; p->xmax = x;
    }

    if(p->ymin > y) p->ymin = y;
    else if(p->ymax < y) p->ymax = y;
    if(p->xmin > x) p->xmin = x;
    else if(p->xmax < x) p->xmax = x;

    // Last pointer
    p->pixelsEnd = p->pixels;
    while(p->pixelsEnd->next != NULL) p->pixelsEnd = p->pixelsEnd->next;

    // Default selection
    if(p->select == NULL) p->select = p->pixels;
}

void PO_draw(Polygon p, Image* img)
{
    if(p->pixels == NULL) return;
    I_changeColor(img, p->color);
    PixelList pl = p->pixels;
    PixelList first = pl;

    // 2 by 2
    while(pl != NULL && pl->next != NULL)
    {
        // Draw
        IM_drawLine(img, 
                pl->p.x, pl->p.y,
                pl->next->p.x, pl->next->p.y);
        // Next
        pl = pl->next;
    }

    // Closing line
    if((p->closed || p->fill ) && first != pl)
        IM_drawLine(img, 
                pl->p.x, pl->p.y,
                first->p.x, first->p.y);

    if(p->fill && p->count > 2) PO_fill(p, img);
}

void PO_drawSelected(Polygon p, Image* img)
{
    if(p->select == NULL) return;
    I_changeColor(img, C_new(1, 0, 0));
    int x, y;
    for(y = p->select->p.y - 3; y <= p->select->p.y + 3; ++y)
    {
        for(x = p->select->p.x - 3; x <= p->select->p.x + 3; ++x)
        {
            I_plot(img, x, y);
        }
    }
    if(p->selectionMode == 'e')
    {
        PixelList next = p->select->next;
        if(next==NULL) next = p->pixels;
        for(y = next->p.y - 3; y <= next->p.y + 3; ++y)
        {
            for(x = next->p.x - 3; x <= next->p.x + 3; ++x)
            {
                I_plot(img, x, y);
            }
        }
        IM_drawLine(img, 
                p->select->p.x, p->select->p.y,
                next->p.x, next->p.y);
    }
}

void PO_fill(Polygon p, Image* img)
{
    int y;
    int x;

    int v; // Intersection buffer

    PixelList pl = p->pixels;
    PixelList first = pl;
    PixelList pixels = NULL;
    Pixel pix;
    for(y = p->ymin; y < p->ymax; ++y)
    {

        pix.y = y;
        // Calcul intersections with segments
        pixels = PO_intersectionPoints(p, y);
        v = 0;
        for(x = p->xmin; x < p->xmax; x++)
        {
            pix.x = x;
            // Test edge intersection
            v += PL_count(pixels, &pix);
            // Test polygon summit intersection
            pl = p->pixels;
            while(pl != NULL)
            {
                if(PX_equal(&pix, &(pl->p)))
                {
                    PixelList previous = pl->previous;
                    if(previous == NULL) previous = p->pixelsEnd;
                    PixelList next = pl->next;
                    if(next == NULL) next = first;


                    // Horizontal line ? Ignore it.
                    if(pl->p.y != next->p.y)
                    {
                        // If horizontal line, previous become the first point not on the line
                        while(previous->p.y == pl->p.y && previous != next)
                        {
                            // Doesnt really matter if the poly is just a straight line
                            previous = previous->previous;
                            if(previous == NULL) previous = p->pixelsEnd;
                        }
                        // Test PI+1 and PI-1
                        if((previous->p.y > y && next->p.y < y) ||
                                (previous->p.y < y && next->p.y > y))
                        {
                            v += 1;
                        }
                    }
                }
                pl = pl->next;
            }

            // Draw if pair
            if(v %2 == 1) 
                I_plot(img, x, y);
        }
        PL_free(pixels);
    }
}


PixelList PO_intersectionPoints(Polygon p, int y)
{
    PixelList res = NULL;

    // Searching
    PixelList pl = p->pixels;
    PixelList first = pl;

    Pixel intersection; // Intersection point
    Pixel a, b;
    a.x = p->xmin - 100; a.y = y;
    b.x = p->xmax + 100; b.y = y;
    PixelList c, d; // Reading from polugon

    // 2 by 2
    while(pl != NULL)
    {
        c = pl;
        if(pl->next == NULL && pl != first) d = first; // Last
        else if(pl->next != NULL) d = pl->next;
        else break;

        if(c->p.y != y && d->p.y != y) // Dont mind of points, only edges
        {
            if(isSegementIntersecting(a, b, d->p, c->p, &intersection))
            {
                PL_addPixel(&res, intersection.x, intersection.y);
            }
        }

        // Next
        pl = pl->next;
    }

    // Ordering
    //res = PL_sortByX(res); 
    // Not usefull in my algorithm

    return res;
}

void PO_selectNext(Polygon p)
{
    if(p->select == NULL) return;
    p->select = p->select->next;
    if(p->select == NULL) p->select = p->pixels;

}

void PO_selectPrevious(Polygon p)
{
    if(p->select == NULL) return;
    p->select = p->select->previous;
    if(p->select == NULL) p->select = p->pixelsEnd;
}

void PO_selectClosestVertex(Polygon p, int x, int y)
{
    p->select = PO_closestVertex(p, x, y);
}

void PO_selectClosestEdge(Polygon p, int x, int y)
{
    if(p->pixels == NULL) return;

    int bestDist = -1;
    
    // 2 by 2
    PixelList pl = p->pixels;
    PixelList a, b;
    while(pl != NULL)
    {
        a = pl;
        b = pl->next;
        if(b == NULL) b = p->pixels;

        int combinedDist = sqrt(
                pow(x - a->p.x, 2) + pow(y - a->p.y, 2)) + 
            sqrt(pow(x - b->p.x, 2) + pow(y - b->p.y, 2));

        if (combinedDist < bestDist || bestDist == -1)
        {
            p->select = a;
            bestDist = combinedDist;
        }

        pl = pl->next;
    }
}

void PO_translateSelected(Polygon p, int x, int y)
{
    if(p->select == NULL) return;
    p->select->p.x += x;
    p->select->p.y += y;

    // BBOx update
    if(p->xmin > p->select->p.x) p->xmin = p->select->p.x;
    else if(p->xmax < p->select->p.x) p->xmax = p->select->p.x;

    if(p->ymin > p->select->p.y) p->ymin = p->select->p.y;
    else if(p->ymax < p->select->p.y) p->ymax = p->select->p.y;
}

void PO_deleteSelected(Polygon p)
{
    PixelList pixels = p->pixels;

    while(pixels != NULL)
    {
        if(pixels == p->select)
        {
            // Remove
            if(pixels->previous != NULL)
                pixels->previous->next = pixels->next;
            if(pixels->next != NULL)
                pixels->next->previous = pixels->previous;

            if(pixels == p->pixels)
                p->pixels = p->pixels->next;

            free(pixels);

            p->count -= 1;
            if(p->count == 0) p->pixels = NULL;
            p->select = p->pixels;

            break;
        }

        pixels = pixels->next;
    }
    p->pixelsEnd = p->pixels;
    while(p->pixelsEnd != NULL && p->pixelsEnd->next != NULL) p->pixelsEnd = p->pixelsEnd->next;

}

void PO_splitSelectedEdge(Polygon p)
{
    if(p->selectionMode != 'e') return;

    // Get pixels
    bool end = false;
    PixelList a = p->select;

    PixelList b = p->select->next;
    if(b == NULL) 
    {
        end = true;
        b = p->pixels;
    }

    // Define new
    PixelList newPL = PL_allocate();
    newPL->p.x = (a->p.x + b->p.x) / 2;
    newPL->p.y = (a->p.y + b->p.y) / 2;

    // Add in list
    a->next = newPL;
    newPL->previous = a;
    newPL->next = b;
    if(end)
    {
        newPL->next = NULL;
        p->pixelsEnd = newPL;
    }

}

PixelList PO_closestVertex(Polygon p, int x, int y)
{
    if(p->pixels == NULL) return NULL;
    int dist = sqrt(
            pow(x - p->pixels->p.x, 2) +
            pow(y - p->pixels->p.y, 2)
            );
    int tmpdist;
    PixelList closest = p->pixels;
    PixelList pl = closest->next;
    while(pl != NULL)
    {
        tmpdist = sqrt(
            pow(x - pl->p.x, 2) +
            pow(y - pl->p.y, 2)
            );
        if(tmpdist < dist)
        {
            dist = tmpdist;
            closest = pl;
        }
        pl = pl->next;
    }
    return closest;
}

// --------- Image

void IM_drawLine(Image *img, int xA, int yA, int xB, int yB){

    bool revert = false;
    if(abs(yB - yA) > abs(xB - xA)) revert = true;

    if(revert)
    {
        int tmp = xA;
        xA = yA;
        yA = tmp;

        tmp = xB;
        xB = yB;
        yB = tmp;
    }

    if(xA > xB)
    {
        int tmp = xA;
        xA = xB;
        xB = tmp;

        tmp = yB;
        yB = yA;
        yA = tmp;
    }

    int dx = xB - xA, dy = abs(yB - yA);
    int err = dx / 2;
    int ystep = 1;
    if(yA > yB) ystep = -1;
    int x = xA, y = yA;

    while (x < xB)
    {
        if(x > img->_width - 1 || y > img->_height - 1) return;

        if(!revert)
            I_plot(img, x, y);
        else
            I_plot(img, y, x);

        err = err - dy;
    
        if(err < 0)
        {
            y += ystep;
            err += dx;
        }

        x++;
    }

}

// --------- Utils


bool isSegementIntersecting(Pixel a, Pixel b, Pixel c, Pixel d, Pixel *out)
{
    //return det(a, b, c) * det(a, b, d) < 0 && det(c, d, a) * det(c, d, b) < 0;
    Pixel r = {b.x - a.x, b.y - a.y};
    Pixel s = {d.x - c.x, d.y - c.y};
    Pixel ca = {c.x - a.x, c.y - a.y};
    float rXs = r.x * s.y - r.y * s.x;
    float caXr = ca.x * r.y - ca.y * r.x;
    if((int)rXs == 0 && (int)caXr == 0) // Colinear
    {
        int caPr = ca.x * r.x + ca.y * r.y;
        Pixel ac = {a.x - c.x, a.y - c.y};
        int acPs = ac.x * s.x + ac.y + s.y;
        int rr = r.x * r.x + r.y * r.y;
        int ss = s.x * s.x + s.y * s.y;
        if( 
            (0 <= caPr && caPr <= rr) || 
            (0 <= acPs && acPs <= ss)
          )
        {
            if(c.x < a.x) out->x = c.x;
            else out->x = a.x;
            out->y = a.y;
            return true;
        }
        return false;

    }
    if((int)rXs == 0 && (int)caXr != 0) return false;

    float caXs = ca.x * s.y - ca.y * s.x;
    float t = caXs / rXs;
    float u = caXr / rXs;

    if((int)rXs != 0 && (0 <= t && t <= 1) && (0 <= u && u <= 1))
    {
        out->x = a.x + t * r.x + 1;
        out->y = a.y;
        return true;
    }


    return false;
}

