#include <stdio.h>
#include <GL/glew.h>
#include <GL/glut.h>
#include <GL/gl.h>

#include "Image.h"
#include "Polygon.h"


Image *img, * img_in;
Polygon poly;
char mode = 'a';
Color bg;

void update()
{
    I_fill(img, bg);
    PO_draw(poly, img);
    if(mode == 'v' || mode == 'e') PO_drawSelected(poly, img);
}

void display_CB()
{
    glClear(GL_COLOR_BUFFER_BIT);
    I_draw(img);
    glutSwapBuffers();
}

void mouse_CB(int button, int state, int x, int y)
{
    if((button==GLUT_LEFT_BUTTON)&&(state==GLUT_DOWN)) // Left click
    {
        if(mode == 'a') // Add
        {
            PO_addPixel(poly, x, img->_height-y);
        }
        else if(mode == 'v') // Point selection
        {
            PO_selectClosestVertex(poly, x, img->_height - y);
        }
        else if(mode == 'e')
        {
            PO_selectClosestEdge(poly, x, img->_height - y);
        }
        update();
    }
    else if( button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN && mode == 'e')
    {
        PO_splitSelectedEdge(poly);
        update();
    }
    glutPostRedisplay();
}

void keyboard_CB(unsigned char key, int x, int y)
{
	switch(key)
	{
            case 27 : exit(1); break;
            case 127: // Delete
                      if(mode == 'v')
                      {
                          PO_deleteSelected(poly);
                          update();
                      }
                      break;
            case 'a':
                      mode = 'a';
                      update();
                      break;
            case 'v':
                      mode = 'v';
                      poly->selectionMode = 'v';
                      update();
                      break;
            case 'e':
                      mode = 'e';
                      poly->selectionMode = 'e';
                      update();
                      break;
            case 'c': 
                      poly->closed = !poly->closed; 
                      if(!poly->closed) poly->fill = false; 
                      update(); 
                      break;
            case 'f': 
                      poly->closed = true; 
                      poly->fill = !poly->fill; 
                      update(); 
                      break;
            default : fprintf(stderr,"keyboard_CB : %d : unknown key.\n",key);
	}
	glutPostRedisplay();
}

void special_CB(int key, int x, int y)
{
	// int mod = glutGetModifiers();

	static int d = 2;

	switch(key)
	{
            case GLUT_KEY_UP    : 
                if(mode == 'v')
                {
                    PO_translateSelected(poly, 0, d);
                    update();
                }
                break;
            case GLUT_KEY_DOWN  : 
                if(mode == 'v')
                {
                    PO_translateSelected(poly, 0, -d);
                    update();
                }
                break;
            case GLUT_KEY_LEFT  : 
                if(mode == 'v')
                {
                    PO_translateSelected(poly, -d, 0);
                    update();
                }
                break;
            case GLUT_KEY_RIGHT :
                if(mode == 'v')
                {
                    PO_translateSelected(poly, d, 0);
                    update();
                }
                break;
            case GLUT_KEY_PAGE_UP : 
                if(mode == 'v' || mode == 'e')
                {
                    PO_selectNext(poly);
                    update();
                }
                break;
            case GLUT_KEY_PAGE_DOWN :
                if(mode == 'v' || mode == 'e')
                {
                    PO_selectPrevious(poly);
                    update();
                }
                break;
            default : fprintf(stderr,"special_CB : %d : unknown key.\n",key);
	}
	glutPostRedisplay();
}

//------------------------------------------------------------------------

int main(int argc, char **argv)
{
	if(argc!=3)
	{
		fprintf(stderr,"\n\nUsage \t: %s <width> <height>\n",argv[0]);
		exit(1);
	}
        int largeur, hauteur;
        largeur = atoi(argv[1]);
        hauteur = atoi(argv[2]);
        img = I_new(largeur,hauteur);			
        poly = PO_allocate();
        bg = C_new(.2, .2, .2);

        int windowPosX = 100, windowPosY = 100;

        glutInitWindowSize(largeur,hauteur);
        glutInitWindowPosition(windowPosX,windowPosY);
        glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE );
        glutInit(&argc, argv);
        glutCreateWindow("float: ploat");

        glViewport(0, 0, largeur, hauteur);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        glOrtho(0,largeur,0,hauteur,-1,1);

        glutDisplayFunc(display_CB);
        glutKeyboardFunc(keyboard_CB);
        glutSpecialFunc(special_CB);
        glutMouseFunc(mouse_CB);
        // glutMotionFunc(mouse_move_CB);
        // glutPassiveMotionFunc(passive_mouse_move_CB);

        update();
        glutMainLoop();
        PO_free(poly);

        return 0;
}
